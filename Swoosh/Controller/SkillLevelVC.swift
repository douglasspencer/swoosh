//
//  SkillLevelVC.swift
//  Swoosh
//
//  Created by Douglas Spencer on 7/11/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class SkillLevelVC: UIViewController {
    
    
    var player: Player!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack_Pressed(_ sender: Any) {
        self.dismiss(animated: true) {
        }
    }
    
}
