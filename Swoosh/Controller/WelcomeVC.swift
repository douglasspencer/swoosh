//
//  ViewController.swift
//  Swoosh
//
//  Created by Douglas Spencer on 7/11/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {
    @IBOutlet weak var imgSwooshLogo: UIImageView!
    @IBOutlet weak var vwBackground: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    @IBAction func btnNext_Pressed(_ sender: Any) {
        performSegue(withIdentifier: "LeagueVC", sender: nil)
    }
    
}

