//
//  LeagueVC.swift
//  Swoosh
//
//  Created by Douglas Spencer on 7/11/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

enum LeagueType {
    case Men
    case Women
    case Coed
}

class LeagueVC: UIViewController {
    
    var player: Player!

    @IBOutlet weak var btnNext: isBorderedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        player = Player()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let SkillVC = segue.destination as? SkillLevelVC {
            SkillVC.player = self.player
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnNext.alpha = 0.0
    }
    
    @IBAction func btnNext_Pressed(_ sender: Any) {
        performSegue(withIdentifier: "SkillLevelVC", sender: nil)
    }
    
    @IBAction func btnBack_Pressed(_ sender: Any) {
        self.dismiss(animated: true) {
        }
    }
    
    func selectLeague(forLeagueType leagueType: LeagueType) {
        player.desiredLeague = leagueType
        btnNext.FadeTo(alphaValue: 1.0, duration: 1.0)
    }
    
    @IBAction func btnMens_Pressed(_ sender: Any) {
        selectLeague(forLeagueType: .Men )
    }
    
    @IBAction func btnWomens_Pressed(_ sender: Any) {
        selectLeague(forLeagueType: .Women)
    }
    
    @IBAction func btnCoed_Pressed(_ sender: Any) {
        selectLeague(forLeagueType: .Coed)
    }
    
    
    
    
}
