//
//  isBorderedButton.swift
//  Swoosh
//
//  Created by Douglas Spencer on 7/11/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit

@IBDesignable
class isBorderedButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 2.5
    }
    
    func FadeTo(alphaValue: CGFloat, duration: CGFloat) {
        UIView.animate(withDuration: TimeInterval(duration)) {
            self.alpha = alphaValue
        }
    }
    
    

}
