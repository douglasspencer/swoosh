//
//  Player.swift
//  Swoosh
//
//  Created by Douglas Spencer on 7/11/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import Foundation

struct Player {
    var desiredLeague: LeagueType?
    var selectedSkillLevel: String?
}
